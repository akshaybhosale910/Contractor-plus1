<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/login.css">
    <title>SignUp</title>
  </head>
  <body>
  
    <img class="wave" src="<?php echo base_url(); ?>assets/wave.png">
	<div class="container">
		<div class="img">
			<img src="<?php echo base_url(); ?>assets/bg.svg" width="500">
		</div>
		<div class="login-content">
      <?php echo form_open('register'); ?>
			<form method="post" action="<?php echo base_url(); ?>register">
				<img src="<?php echo base_url(); ?>assets/avatar.svg" height="100">
				<h2 class="title">Welcome</h2>
           		<div class="input-div one">
           		    <div class="div mb-2"> 
                    <input type="email" name="UserEmail" value="<?php echo $this->session->userdata('UserEmail1'); ?>" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" readonly>
                 </div>

           		  <div class="div mb-2"> 
                    <input type="text" value="<?php echo set_value('FirstName'); ?>" name="FirstName" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" placeholder="First Name" required>
                    <?php echo form_error('FirstName','<label class="text-danger" style="font-size: 11px;">','</label>'); ?>
                 </div>

                 <div class="div mb-2"> 
                    <input type="text" value="<?php echo set_value('LastName'); ?>" name="LastName" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" placeholder="Last Name" required>
                    <?php echo form_error('LastName','<label class="text-danger" style="font-size: 11px;">','</label>'); ?>
                 </div>

                
                 <div class="div mb-2"> 
                    <input type="password" value="<?php echo set_value('Password'); ?>" name="Password" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" placeholder="Password" required>
                    <?php echo form_error('Password','<label class="text-danger" style="font-size: 11px;">','</label>'); ?>
                 </div>
           		</div>
           	            	
            	<input type="submit" class="btn" value="Login">
            <?php echo form_close(); ?>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/Bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>