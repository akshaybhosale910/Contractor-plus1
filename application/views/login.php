<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/Bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/Bootstrap/dist/css/login.css">
    <title>Login</title>
  </head>
  <body>
  
    <img class="wave" src="assets/wave.png">
	<div class="container">
		<div class="img">
			<img src="assets/bg.svg" width="500">
		</div>
		<div class="login-content">
      <?php 
            echo form_open('verifymail'); 
      ?>
			<!-- <form method="post" action="<?php echo base_url(); ?>verifymail"> -->
				<img src="assets/avatar.svg" height="100">
				<h2 class="title">Welcome</h2>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div"> 
           		   		<input type="text" name="UserEmail" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" placeholder="Email Address">
           		   </div>
           		</div>
           		
            	<input type="submit" class="btn" value="Login">
              <label class="text-danger"><?php echo form_error('UserEmail'); ?></label>
            <?php echo form_close(); ?>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="assets/Bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>