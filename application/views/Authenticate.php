<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/login.css">
    <title>Login</title>
  </head>
  <body>
  
    <img class="wave" src="<?php echo base_url(); ?>assets/wave.png">
	<div class="container">
		<div class="img">
			<img src="<?php echo base_url(); ?>assets/bg.svg" width="500">
		</div>
		<div class="login-content">
      <?php echo form_open('verifyauth'); ?>
			<form method="post" action="<?php echo base_url(); ?>verifyauth">
				<img src="<?php echo base_url(); ?>assets/avatar.svg" height="100">
				<h2 class="title">Welcome</h2>
           		<div class="input-div one">
           		  
                <div class="div"> 
                    <input type="email" name="email" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" value="<?php echo $this->session->userdata('UserEmail1'); ?>" hidden>
                 </div>
           		   <div class="div"> 
           		   		<input type="password" value="" name="Password" class="form-control" style="border: 2px solid #32be8f; border-radius: 22px;" placeholder="Password" required>

           		   </div>
           		</div>
           	            	
            	<input type="submit" class="btn" value="Login">
              <?php echo $this->session->flashdata('alert'); ?>
               <label class="text-danger"><?php echo form_error('Password'); ?></label>

           <?php echo form_close(); ?>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/Bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>