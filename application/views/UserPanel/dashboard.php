<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/dashboard.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/Bootstrap/dist/css/login.css">
    <title>Dashboard</title>
  </head>
  <body>
  
    <div class="wrapper">
    <div class="sidebar">
      <div class="bg_shadow"></div>
        <div class="sidebar__inner">
           <div class="close">
          <i class="fas fa-times"></i>
        </div>
        <div class="profile_info">
            <div class="profile_img">
              <img src="<?php if($UserData->Avatar) echo base_url().$UserData->Avatar; else echo'http://localhost/Contractor-plus/assets/avatar.svg'; ?>" alt="profile_img">
            </div>
            <div class="profile_data">
                <p class="name"><?php echo $this->session->userdata('FirstName'); ?></p>  
               
            </div>
        </div>
        <ul class="siderbar_menu">
            <li><a href="#">
              <div class="icon"><i class="fas fa-laptop"></i></div>
              <div class="title">Dashboard</div>
              </a></li>  
          <li><a href="#" class="active">
              <div class="icon"><i class="fas fa-newspaper"></i></div>
              <div class="title">Profile</div>
              </a></li>  
          <li><a href="#">
              <div class="icon"><i class="fas fa-file-alt"></i></div>
              <div class="title">Documents</div>
              </a></li>  
          <li><a href="#">
              <div class="icon"><i class="fas fa-cog"></i></div>
              <div class="title">Settings</div>
              </a></li>  
          <li><a href="#">
              <div class="icon"><i class="fas fa-question-circle"></i></div>
              <div class="title">Help</div>
              </a></li>  
        </ul>
      </div>
    </div>
    <div class="main_container">
      <div class="top_navbar">
          <div class="hamburger">
              <div class="hamburger__inner">
                  <i class="fas fa-bars"></i>  
              </div>  
          </div>
         <ul class="menu">
            <li class="mt-3">Contractor Plus</li>
            
         </ul>
         <ul class="right_bar">
            <li><i class="fas fa-sign-out-alt"> <a href="<?php echo base_url(); ?>/logout">Logout</a></i></li> 
         </ul>
      </div>
      
 <div class="container" style="display: inline-block;">

 	<div class="">
        <div class="row">
        	<div class="offset-1 col-md-10">
        		<h4><?php echo $this->session->flashdata('error'); ?></h4>
        		<div class="card">
  					<div class="card-header"> User Profile </div>
  					<div class="card-body">
     <form action="<?php echo base_url().'update/'.$UserData->UserId; ?>" method="post" enctype="multipart/form-data" style="width: 100%;">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Name</label>
      <input type="text" class="form-control" name="FullName" value="<?php echo $UserData->FirstName.' '.$UserData->LastName; ?>" readonly>

    </div>
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control" value="<?php echo $UserData->UserEmail; ?>" readonly>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Profile Picture</label>
    <input type="file" class="form-control" name="Avatar" required>
    <?php echo form_error('Avatar','<label class="text-danger" style="font-size: 11px;">','</label>'); ?>
    </div>
    
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Department</label>
      <select id="Department" name="Department" class="form-control">
      	<option></option>
        <?php 
       foreach($Department as $Dept) { ?>
             <option value="<?php echo $Dept->DepartmentId ;?>"
          <?php if ($Dept->DepartmentName == $UserData->Department) : ?> selected<?php endif; ?>>
             <?php echo $Dept->DepartmentName; ?>
               </option>
      <?php    } ?> 
        	
      </select>
      <?php echo form_error('Department','<label class="text-danger" style="font-size: 11px;">','</label>'); ?>
    </div>
    <div class="form-group col-md-6">
      <label>Sub Department</label>
      <select id="SubDepartment" name="SubDepartment" class="form-control">
       <option><?php echo $UserData->SubDepartment; ?></option>
      </select>
      <?php echo form_error('SubDepartment','<label class="text-danger" style="font-size: 11px;">','</label>'); ?>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Update Profile</button>
</form>
  					</div>
				</div>
        	</div>
        	
        </div>
    </div>
      

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/Bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <script>
    	$(document).ready(function(){
    $("#Department").change(function(){
    	
    	
       var id =  $('#Department').val();
      

       $.ajax({
      type: 'POST',
      url: "<?php echo base_url().'login/SubDepartment' ?>",
      data: {id:id},
      success:function(response){
         $('#SubDepartment').html(response);

       }
       

    });

  });
});
    </script>
  </body>
</html>