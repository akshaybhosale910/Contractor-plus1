<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {


	public function EmailVerify($email)
	{
		$this->db->where('UserEmail',$email);
    $query = $this->db->get('ci_usertable');
    return $query->row();
	}

  public function Department()
  {
    $query = $this->db->get('ci_departments');
    return $query->result(); 
  }
  
  function SubDepartment($id)
  {
    $this->db->where('DepartmentId',$id);
    $query = $this->db->get('ci_sub_department');
    return $query->result();
  }

   public function Auth($email,$password)
  {
    $this->db->where('UserEmail',$email);
    $this->db->where('Password',md5($password));
    $query = $this->db->get('ci_usertable');
      return $query->row();

    
  }
  public function DepartmentID($id)
  {
    $this->db->where('DepartmentId',$id);
    $query = $this->db->get('ci_departments');
    return $query->row(); 
  }

  
}
?>