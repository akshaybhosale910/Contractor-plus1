<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logfirst extends CI_controller{

	public function log()
	{

		if (!$this->session->userdata('UserEmail')) {
			$this->load->view('login');
		}
	}
	
}
