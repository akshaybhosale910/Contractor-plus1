<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() 
	{
		parent::__construct(); 
$this->load->library('session');

		$this->load->model('LoginModel');
		if ($this->session->userdata('UserEmail')) {
			$data['UserData'] = $this->LoginModel->EmailVerify($this->session->userdata('UserEmail'));

			$data['Department'] = $this->LoginModel->Department();
			$this->data = $data;
		}
		
	}

	public function index()
	{
		$this->load->view('login');
	}
	public function dashboard()
	{
		
		if ($this->session->userdata('UserEmail')) {
			$data['UserData'] = $this->LoginModel->EmailVerify($this->session->userdata('UserEmail'));

			$data['Department'] = $this->LoginModel->Department();
			$this->load->view('UserPanel/dashboard',$data);
		}
		else{
			
			redirect(base_url().'login');
		}
	}

	public function Authenticate()
	{
		$this->load->view('Authenticate');
	}

	public function SignUp()
	{
		$this->load->view('SignUp');
	}

	public function VerifyMail()
	{
		$this->form_validation->set_rules('UserEmail', 'UserEmail', 'trim|required|valid_email');
		
		if($this->form_validation->run())  
		{   
		$UserEmail = $this->input->post('UserEmail');
		$data = $this->LoginModel->EmailVerify($UserEmail);
		$this->session->set_userdata('UserEmail1', $UserEmail);
		if($data->UserEmail)
		{	

			redirect(base_url().'auth');
		}
		else{
			
			
			redirect(base_url().'signup');

		}
		}
		else{
			$this->load->view('login');
		}
	}
	public function VerifyAuth()
	{
		$this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[8]');
		
		if($this->form_validation->run())  
		{   
			$email = $this->input->post('email');  
			$password = $this->input->post('Password');
			$data= $this->LoginModel->Auth($email, $password);

			if($data)  
			{  

				$this->session->unset_userdata('UserEmail');
				$session = array(  
					'UserEmail' =>  $data->UserEmail, 
					'Avatar' => $data->Avatar
				); 
				$this->session->set_userdata($session);  
				redirect(base_url() . 'dashboard');  
			}else{   
				$alert = '<div class="alert alert-danger" role="alert">
				<strong>Opps !</strong> You Entered Wrong Password.
				</div>';
				$this->session->set_flashdata('alert', $alert);  
				redirect(base_url().'auth');  
			}  
		}
		else{
			$this->load->view('Authenticate');
		}
	}

	public function RegisterEmail()
	{
		
		$this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('FirstName', 'FirstName', 'required|alpha');
		$this->form_validation->set_rules('LastName', 'LastName', 'required|alpha');
		
		if($this->form_validation->run())  
		{   
		$data = array(
			'FirstName' => $this->input->post('FirstName'),
			'LastName'  =>$this->input->post('LastName'),
			'UserEmail'  => $this->input->post('UserEmail'),
			'Password' => md5($this->input->post('Password'))
		);
		$this->db->insert('ci_usertable',$data);
		$alert = '<div class="alert alert-success" role="alert"> '.$data['FirstName'].' '.$data['LastName'].' can successfully registered</div>';
		$this->session->set_flashdata('alert', $alert);
		$this->session->set_userdata($data);
		redirect(base_url().'dashboard');
	}
	else{
		$this->load->view('SignUp');
	}
	}

	

	public function SubDepartment()
	{
		$id = $this->input->post('id');
		$data = $this->LoginModel->SubDepartment($id);
		$output = array();
		foreach($data as $row){
			echo '<option>'.$row->SubDeptName.'</option>';
		}
		echo json_encode($data);
	}

	public function ProfileUpdate($id)
	{


		$this->form_validation->set_rules('Department', 'Department', 'required');
		$this->form_validation->set_rules('SubDepartment', 'SubDepartment', 'required');
		
		
		if($this->form_validation->run())  
		{   
		$DeptId = $this->input->post('Department');

		$data['Department'] = $this->LoginModel->DepartmentID($DeptId);
		
		$config['upload_path']      = 'assets/';
		$config['allowed_types']    = 'jpg|png|jpeg';
		$config['file_name']        = 'UserID_'.$id;     	
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if($this->upload->do_upload('Avatar'))
		{               
			$Avatar =  $this->upload->data();
			$img_path= $config['upload_path'].$config['file_name'].$Avatar['file_ext'];

			$data = array(
				'Avatar' => $img_path,
				'Department' => $data['Department']->DepartmentName,
				'SubDepartment' => $this->input->post('SubDepartment')
			);

		}else{
			echo $this->upload->display_errors();
			exit;
			$data = array(
				'Department' => $data['Department']->DepartmentName,
				'SubDepartment' => $this->input->post('SubDepartment')
			); 
		}
		$this->db->where('UserId',$id);
		$this->db->set($data);
		$this->db->update('ci_usertable');
		$alert = '<div class="alert alert-success" role="alert"> '.$this->session->userdata('UserEmail').'  Update Successfully</div>';
		$this->session->set_flashdata('alert', $alert);
		redirect(base_url().'/dashboard');
	}
	else {
		$this->dashboard();
	}
		

	}
	public function logout()
	{
		$this->session->unset_userdata('UserEmail');
		redirect(base_url().'login');
	}
}
