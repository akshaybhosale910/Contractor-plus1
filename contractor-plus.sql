-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2021 at 03:28 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contractor-plus`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_departments`
--

CREATE TABLE `ci_departments` (
  `DepartmentId` int(12) NOT NULL,
  `DepartmentName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ci_departments`
--

INSERT INTO `ci_departments` (`DepartmentId`, `DepartmentName`) VALUES
(1, 'Information Technology '),
(2, 'Accounts'),
(3, 'Loan Department'),
(4, 'Medical');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sub_department`
--

CREATE TABLE `ci_sub_department` (
  `SubDeptId` int(12) NOT NULL,
  `SubDeptName` varchar(255) NOT NULL,
  `DepartmentId` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ci_sub_department`
--

INSERT INTO `ci_sub_department` (`SubDeptId`, `SubDeptName`, `DepartmentId`) VALUES
(1, 'Web Developer', 1),
(2, 'Software Developer', 1),
(3, 'Automated Tester', 1),
(4, 'Jr Accountant', 2),
(5, 'Sr Accountant', 2),
(6, 'Home Loan', 3),
(7, 'Car Loan', 3),
(8, 'Pharmacist', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ci_usertable`
--

CREATE TABLE `ci_usertable` (
  `UserId` int(12) NOT NULL,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UserEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SubDepartment` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_usertable`
--

INSERT INTO `ci_usertable` (`UserId`, `FirstName`, `LastName`, `UserEmail`, `Password`, `Avatar`, `Department`, `SubDepartment`) VALUES
(10, 'Rajesh', 'Shelar', 'ajit1@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'assets/UserID_10.jpg', 'Information Technology ', 'Web Developer'),
(17, 'Rajesh', 'Shelar', 'ajit@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'assets/UserID_17.jpg', 'Information Technology ', 'Web Developer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_departments`
--
ALTER TABLE `ci_departments`
  ADD PRIMARY KEY (`DepartmentId`);

--
-- Indexes for table `ci_sub_department`
--
ALTER TABLE `ci_sub_department`
  ADD PRIMARY KEY (`SubDeptId`);

--
-- Indexes for table `ci_usertable`
--
ALTER TABLE `ci_usertable`
  ADD PRIMARY KEY (`UserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_departments`
--
ALTER TABLE `ci_departments`
  MODIFY `DepartmentId` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ci_sub_department`
--
ALTER TABLE `ci_sub_department`
  MODIFY `SubDeptId` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ci_usertable`
--
ALTER TABLE `ci_usertable`
  MODIFY `UserId` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
